import { combineReducers } from 'redux';
import { reducer as formReduser } from 'redux-form';
import PostsReducer from './reducer_posts';

const rootReducer = combineReducers({
  posts:  PostsReducer,
  form:   formReduser
});

export default rootReducer;
