# What's this?

Homework for [Modern React with Redux](https://www.udemy.com/react-redux/) course on Udemy.

###Getting Started###

There are two methods for getting started with apps from this repo.

####Not Familiar with Docker?#####

```
  > cd AppFolder
  > npm install
  > npm start
```

####Familiar with Docker?#####

```
  > cd AppFolder
  > docker-compose up
```
