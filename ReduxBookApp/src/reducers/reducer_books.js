export default function() {
  return [
    { title: 'Javascript: The Good Parts', pages: 101 },
    { title: 'Harry Potter', pages: 52 },
    { title: 'The Dark Tower', pages: 1250 },
    { title: 'Eloquent Ruby', pages: 25 }
  ];
}
